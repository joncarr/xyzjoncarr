package routes

import (
	"net/http"
	"xyzjoncarr/pkg/types"
	"xyzjoncarr/system/store"
	"xyzjoncarr/web/controllers/admin"
)

// AdminMiddleware is middleware specifically for admin routes
func AdminMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
	})
}

// GetSubRoutes returns a map of string keys and SubRoutePackage values.
//
func GetSubRoutes(db store.Database) map[string]types.SubRoutePackage {

	adminC := admin.NewController(db)

	subRoutes := map[string]types.SubRoutePackage{
		"/admin": types.SubRoutePackage{
			Routes: types.Routes{
				types.Route{"Dashboard", "GET", "/", adminC.Index},
			},
			Middleware: AdminMiddleware,
		},
	}

	return subRoutes
}
