package routes

import (
	"net/http"
	"xyzjoncarr/pkg/types"
	"xyzjoncarr/system/store"
	"xyzjoncarr/web/controllers/home"
)

// GlobalMiddleware is the golbal middleware for your application
// TODO: consider a middleware type and cleaner middleware solution
func GlobalMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
	})
}

// GetRoutes returns a slice of routes your application will use.
// All routes should be created here.
func GetRoutes(db store.Database) types.Routes {

	homeC := home.NewController(db)

	// Create your routes in this slice
	return types.Routes{
		types.Route{"Home", "GET", "/", homeC.Index},
	}
}
