package home

import (
	"fmt"
	"net/http"
	"xyzjoncarr/pkg/base"
	"xyzjoncarr/system/store"
)

var db store.Database

// Controller for application home
type Controller struct {
	base.Static
	db store.Database
	// IndexPage *views.View
}

// NewController builds and returns a pointer to a controller
func NewController(database store.Database) *Controller {
	db = database
	return &Controller{
		db: db,
	}
}

// Index action for the home controller
func (c *Controller) Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "This is the homepage")
}
