package controllers

import (
	"xyzjoncarr/pkg/base"
)

// StaticController handles static pages
type StaticController struct {
	base.Static
	// Static pages go here
	// i.e.
	// Home *views.View
}

// NewStaticController constructs and returns a StaticController
func NewStaticController() *StaticController {
	return &StaticController{
		// Static page assignments
		// i.e for a homepage using the main layout where
		// the template file is located in static directory
		// and is named "home.gohtml"
		// Home: views.NewView("main", "static/home")
	}
}
