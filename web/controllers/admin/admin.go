package admin

import (
	"fmt"
	"net/http"
	"xyzjoncarr/pkg/base"
	"xyzjoncarr/system/store"
)

var db store.Database

// Controller for admin related handling
type Controller struct {
	base.Resource
	db store.Database
}

// NewController builds and returns a pointer to a new controller
func NewController(database store.Database) *Controller {
	db = database
	return &Controller{
		db: db,
	}
}

// Index handles the main admin route
func (c *Controller) Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "admin dashboard")
}
