package base

import (
	"net/http"
)

// ResourceController interface
type ResourceController interface {
	// GET
	Index(http.ResponseWriter, *http.Request)
	Create(http.ResponseWriter, *http.Request)
	Show(http.ResponseWriter, *http.Request)
	Edit(http.ResponseWriter, *http.Request)

	// POST
	Store(http.ResponseWriter, *http.Request)

	// PUT/PATCH
	Update(http.ResponseWriter, *http.Request)

	// DELETE
	Destroy(http.ResponseWriter, *http.Request)
}

// Resource is a default implementation of CRUD actions for Resource
// controllers in the event you dont need or want all CRUD action
// implementations.
/*
   type UserController struct {
       base.Resource
       ...
   }
*/
type Resource struct{}

// Index default implementation
// GET /{model}
func (rc *Resource) Index(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "handler not implemented", http.StatusNotFound)
}

// Create default implementation
// GET /{model}/create
func (rc *Resource) Create(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "handler not implemented", http.StatusNotFound)
}

// Show default implementation
// GET /{model}/{id}
func (rc *Resource) Show(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "handler not implemented", http.StatusNotFound)
}

// Edit default implementation
// GET /{model}/{id}/edit
func (rc *Resource) Edit(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "handler not implemented", http.StatusNotFound)
}

// Store default implementation
// POST /{model}
func (rc *Resource) Store(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "handler not implemented", http.StatusNotFound)
}

// Update default implementation
// PUT/PATCH /{model}/{id}
func (rc *Resource) Update(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "handler not implemented", http.StatusNotFound)
}

// Destroy default implementation
// DELETE /{model}/{id}
func (rc *Resource) Destroy(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "handler not implemented", http.StatusNotFound)
}

// StaticController interface
type StaticController interface {
	Index(http.ResponseWriter, *http.Request)
}

// Static handles static pages
type Static struct{}

// Index default implementation for static controllers
func (s *Static) Index(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "handler not implemented", http.StatusNotFound)
}
