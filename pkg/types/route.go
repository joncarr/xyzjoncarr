package types

import "net/http"

// Route defines a url route
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

// Routes is a slice of routes
type Routes []Route

// SubRoutePackage for sub routes
type SubRoutePackage struct {
	Routes     Routes
	Middleware func(http.Handler) http.Handler
}
