package types

// Entity is a cheap shot at faking a type generic
// This exists to aid in the construction of app services
// The default implementation to satisy the interface is
/*
 func (e *types.Entity) TypeString() string {
        return fmt.Sprintf("%T", e)
}
*/
type Entity interface {
	TypeString() string
}
