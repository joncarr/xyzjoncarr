package system

import (
	"log"
	"net/http"

	"xyzjoncarr/internal/config"
	"xyzjoncarr/system/store"
)

// Server is the application server
type Server struct {
	Port     string          `json:"port"`
	Database *store.Instance `json:"database"`
}

// NewServer returns a new server instance
func NewServer() *Server {
	return &Server{}
}

// Init initializes the server. Any initializations for the
// server should happen here.
func (s *Server) Init(bundle *config.Bundle) {
	var err error

	log.Println("Initializing server...")
	s.Port = bundle.AppPort
	s.Database = store.Build()
	s.Database.Meta = bundle.DBConnInfo
	s.Database.Connection, err = store.Connect(s.Database.Meta)
	if err != nil {
		// TODO: gracefully handle this...somehow
		panic(err)
	}

}

// Start starts the server with a new router instance
func (s *Server) Start() error {
	r := NewRouter()
	r.Init(s.Database.Connection)
	log.Println("Server is running on port:", s.Port)
	return http.ListenAndServe(s.Port, r.Router)
}
