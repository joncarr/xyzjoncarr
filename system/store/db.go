package store

import (
	"fmt"
	"io"

	"github.com/jinzhu/gorm"
	// Driver
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var dialectString string

// DialectID type for enumerated database dialect values
type DialectID int

// Database type for clarity and readability
type Database io.Closer

// Enumerated values for database dialects
const (
	Postgres DialectID = iota
	MySQL
	SQLite
	SQLServer
)

// Instance represents the database instance and associated information
// Instance.Connection is representative of the database orm/type used for
// the application.  This is so you are not tied down to any specific
// database implementation.
type Instance struct {
	Meta       ConnectionInfo `json:"meta"`
	Connection Database       `json:"connection"`
}

// ConnectionInfo is a container to hold the information required
// to make a database connection
type ConnectionInfo struct {
	Host       string    `json:"host"`
	Port       int       `json:"port"`
	Name       string    `json:"name"`
	User       string    `json:"user"`
	Password   string    `json:"password"`
	SSL        bool      `json:"ssl"`
	SQLitePath string    `json:"sq_lite_path"`
	Dialect    DialectID `json:"dialect"`
}

// Build constructs and returns a new database instance
func Build() *Instance {
	return &Instance{}
}

// Connect makes a connection to the application database
// TODO (jon) this implementation will most likely need to change as
// to not lock the function into gorm.....
func Connect(ci ConnectionInfo) (Database, error) {
	db, err := gorm.Open(dialectString, buildConnectionString(ci))
	if err != nil {
		return nil, err
	}

	return db, nil
}

func buildConnectionString(ci ConnectionInfo) string {
	switch ci.Dialect {
	case Postgres:
		dialectString = "postgres"
		if ci.SSL {
			return fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s", ci.Host, ci.Port, ci.User, ci.Name, ci.Password)
		}

		return fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s sslmode=disable", ci.Host, ci.Port, ci.User, ci.Name, ci.Password)

	case SQLite:
		dialectString = "mysql3"
		return ci.SQLitePath
	case MySQL:
		dialectString = "mysql"
		return fmt.Sprintf("%s:%s@%s/%s?charset=utf8&parseTime=True&loc=Local", ci.User, ci.Password, ci.Host, ci.Name)
	case SQLServer:
		dialectString = "mssql"
		return fmt.Sprintf("sqlserver://%s:%s@%s:%d?database=%s", ci.User, ci.Password, ci.Host, ci.Port, ci.Name)
	default:
		return ""
	}
}
