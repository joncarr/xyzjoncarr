package system

import (
	"xyzjoncarr/pkg/types"
	"xyzjoncarr/system/store"
	"xyzjoncarr/web/routes"

	"github.com/gorilla/mux"
)

// Router is the application router
type Router struct {
	Router *mux.Router
	Routes types.Routes
}

// AttachSubRouter attaches a subrouter to the application router
// The main use of this is to satisfy a separation of concerns
// philosophy when dealing with application routing.
//
// The parameter "path" is the route prefix (i.e. /admin/*)
// The parameter "subRoutes" are the vailable routes on the subrouter
// The parameter "mw" is the middleware to be used on the subrouter
// Every subrouter group will have it's own middleware set as well as
// the GlobalMiddleware set
func (r *Router) AttachSubRouter(path string, subRoutes types.Routes, mw mux.MiddlewareFunc) *mux.Router {

	subRouter := r.Router.PathPrefix(path).Subrouter()
	subRouter.Use(mw)

	for _, route := range subRoutes {
		subRouter.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}
	return subRouter
}

// Init initializes the router. Any initializations for the router
// should happen here.
func (r *Router) Init(db store.Database) {
	r.Router.Use(routes.GlobalMiddleware)

	appRoutes := routes.GetRoutes(db)

	for _, route := range appRoutes {
		r.Router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}

	subRoutes := routes.GetSubRoutes(db)

	for k, v := range subRoutes {
		r.AttachSubRouter(k, v.Routes, v.Middleware)
	}

}

// NewRouter constructs a router
func NewRouter() (r Router) {
	r.Router = mux.NewRouter().StrictSlash(true)
	return
}
