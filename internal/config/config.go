package config

import (
	"fmt"
	"log"
	"os/exec"
	"xyzjoncarr/system/store"
)

// Bundle holds the configuration data used by the application
type Bundle struct {
	AppName    string               `json:"app_name"`
	AppPort    string               `json:"app_port"`
	DBConnInfo store.ConnectionInfo `json:"db_info"`
	Pepper     string               `json:"pepper"`
	HMACKey    string               `json:"hmac_key"`
}

func normalizePort(port string) string {
	return fmt.Sprintf(":%s", port)
}

////////////////////////////////////////////////////////////////////////////////
// Everything below this is for development purposes only.

// DevBundle is a bundle for devlopment
func DevBundle() *Bundle {
	return &Bundle{
		AppName:    "DevBundle",
		AppPort:    normalizePort("3333"),
		DBConnInfo: defaultDatabase(),
		Pepper:     defaultPepper(),
		HMACKey:    defaultHMAC(),
	}
}

func CreatePostgresDB(b *Bundle) {
	// q := fmt.Sprintf("'create database %s;'", b.DBConnInfo.Name)
	args := []string{"-c", "'create", "database", b.DBConnInfo.Name + ";'", "-U", "postgres"}
	c, err := exec.Command("psql", args...).Output()
	if err != nil {
		log.Println(err)
	}
	fmt.Println(string(c))
}

// defaultDatabase is used mainly for development.
// It will default to a postgres connection with "postgres" as the default user
func defaultDatabase() store.ConnectionInfo {
	return store.ConnectionInfo{
		Host:       "localhost",
		Port:       5432,
		Name:       "test_dev_bundle",
		User:       "postgres",
		Password:   "postgres",
		SSL:        false,
		SQLitePath: "",
		Dialect:    store.Postgres,
	}
}

// defaultPepper is for development purposes only
// TODO remove when generator is complete
func defaultPepper() string {
	return "7bdd1168-f27a-4977-9bc2-90cf186e0907"
}

// defaultHMAC is for development purposes only
// TODO remove when generator is complete
func defaultHMAC() string {
	return "ba474602-09c1-4362-8100-ef9861f5770c "
}
