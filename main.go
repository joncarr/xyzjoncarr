package main

import (
	"log"
	"xyzjoncarr/internal/config"
	"xyzjoncarr/system"
)

func main() {
	dBun := config.DevBundle()

	// config.CreatePostgresDB(dBun)

	// Dev only, remove to commandline tool later
	// c := exec.Command("/bin/sh", "internal/scripts/create_postgres_db.sh")
	// if err := c.Run(); err != nil {
	// 	log.Println(err)
	// }

	svr := system.NewServer()
	svr.Init(dBun)
	log.Fatal(svr.Start())
}
